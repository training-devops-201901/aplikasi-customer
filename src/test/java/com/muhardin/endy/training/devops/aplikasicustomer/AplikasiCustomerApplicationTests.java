package com.muhardin.endy.training.devops.aplikasicustomer;

import com.muhardin.endy.training.devops.aplikasicustomer.dao.CustomerDao;
import com.muhardin.endy.training.devops.aplikasicustomer.entity.Customer;
import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(
    scripts = {"classpath:/delete-data-customer.sql", "classpath:/sample-customer.sql"},
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
)
public class AplikasiCustomerApplicationTests {

        @Autowired private CustomerDao customerDao;
    
	@Test
	public void testFindCustomerById() {
            Customer c = customerDao.findById("u001").get();
            Assert.assertNotNull("Customer u001 harusnya ada di database", c);
	}

}
